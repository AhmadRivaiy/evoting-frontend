import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  Dimensions,
  ImageBackground,
  StatusBar,
  Image
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { Container, Button, Drawer, List, ListItem, Content, Text } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import AsyncStorage from '@react-native-community/async-storage';
import oAuthFirebase from '@react-native-firebase/app'
import { RoutingHelper } from './src/helpers/RoutingHelper';
import bgDrawer from './src/assets/images/bgDrawer2.png';
import { handleGetItem } from './src/components/storeAsync.storage';

import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';

class drawerContentComponents extends Component {
  constructor(props) {
    super(props);
    this._isMounted = false;
    this.state = {
      nama: '',
      nisn: '',
      isVoted: '0'
    }
  }

  navigateToScreen = (route) => (
    () => {
      const navigateAction = NavigationActions.navigate({
        routeName: route
      });
      new Promise(() => {
        setTimeout(() => {
          this.props.navigation.navigate(navigateAction);
        }, 200);
      })
      this.props.navigation.closeDrawer()
    })

  componentDidMount() {
    this._isMounted = true;
    this._isMounted && this.getDataListener();
  }

  async getDataListener() {
    try {
      await handleGetItem('currentUser')
        .then(x => {
          this.setState({
            nama: x.payload.nama,
            nisn: x.payload.nisn,
            isVoted: x.is_voted
          })
        }).catch(err => {
          console.log(err)
        })

      await handleGetItem('currentCandidateVoted')
        .then(x => {
          this.setState({
            namaKandidat: x.nama,
            imageKandidat: x.image
          })
        }).catch(err => {
          this.setState({
            namaKandidat: '-',
            imageKandidat: null
          })
        })
    } catch (e) {
      console.log('Dashboard')
      console.log(e)
    }
  }

  async handleLogout() {
    const value = await AsyncStorage.getItem('isFirebase')
    if( value !== null){
      await oAuthFirebase.auth().signOut();
      await AsyncStorage.clear();
      this.props.navigation.dispatch(RoutingHelper.finishActivity('Splash'))
    }else{
      await AsyncStorage.clear();
      this.props.navigation.dispatch(RoutingHelper.finishActivity('Splash'))
    }
  }

  render() {
    const { nama, nisn, isVoted, namaKandidat, imageKandidat } = this.state;
    return (
      <Container style={{ backgroundColor: '#f2f2f2' }}>
        {this.props.navigation.state.isDrawerOpen ? <StatusBar translucent backgroundColor="transparent" barStyle="dark-content" /> : <StatusBar translucent backgroundColor="transparent" barStyle="light-content" />}
        <ImageBackground
          style={{ padding: 25 }}
          source={bgDrawer}
        >
          <Text style={{ marginTop: getStatusBarHeight(), fontSize: 17, fontFamily: 'ProductSans-Bold', color: '#000' }}>
            {nama}
          </Text>
          <Text style={{ fontSize: 12, fontFamily: 'GoogleSans-Regular', color: '#94a3a5' }}>
            {nisn}
          </Text>
          <Text style={{ fontSize: 12, fontFamily: 'GoogleSans-Regular', color: '#94a3a5', alignSelf: 'flex-end', marginTop: 15 }}>
            Status Vote
          </Text>
          <Text style={{ fontSize: 12, fontFamily: 'ProductSans-Bold', color: '#0F9797', alignSelf: 'flex-end', fontWeight: 'bold' }}>
            {isVoted == '1' ? 'Sudah Digunakan' : 'Anda Belum Memilih'}
          </Text>
        </ImageBackground>

        <Content>
          <List>
            <ListItem>
              <TouchableOpacity
                style={{ width: Dimensions.get('screen').width }}
                onPress={this.navigateToScreen('QuickCount')}
              >
                <Grid>
                  <Col style={{justifyContent:'center', padding: 10, alignItems: 'center'}} size={0.5}>
                    <FontAwesome5 name="vote-yea" size={24} color="#1d959e" />
                  </Col>
                  <Col size={6} style={{justifyContent:'center'}}>
                    <Text style={{ color: '#000', fontSize: 12, fontFamily: 'GoogleSans-Regular', fontWeight: 'bold', textAlign: 'center', alignSelf:'flex-start'}}> Quick Count </Text>
                  </Col>
                </Grid>
              </TouchableOpacity>
            </ListItem>
            <ListItem>
              <TouchableOpacity
                style={{ width: Dimensions.get('screen').width }}
              >
                <Grid>
                  <Col style={{ justifyContent:'center',  padding: 10, alignItems: 'center'}} size={0.5}>
                    <FontAwesome5 name="question" size={24} color="#1d959e" />
                  </Col>
                  <Col size={6} style={{justifyContent:'center'}}>
                    <Text style={{ color: '#000', fontSize: 12, fontFamily: 'GoogleSans-Regular', fontWeight: 'bold', textAlign: 'center', alignSelf:'flex-start'}}> About </Text>
                  </Col>
                </Grid>
              </TouchableOpacity>
            </ListItem>
            <ListItem>
              <TouchableOpacity
                style={{ width: Dimensions.get('screen').width }}
                onPress={() => this.handleLogout()}
              >
                <Grid>
                  <Col style={{ justifyContent:'center',  padding: 10, alignItems: 'center'}} size={0.5}>
                    <SimpleLineIcons name="logout" size={24} color="#E65A76" />
                  </Col>
                  <Col size={6} style={{justifyContent:'center'}}>
                    <Text style={{ color: '#000', fontSize: 12, fontFamily: 'GoogleSans-Regular', fontWeight: 'bold', textAlign: 'center', alignSelf:'flex-start'}}> Logout </Text>
                  </Col>
                </Grid>
              </TouchableOpacity>
            </ListItem>
          </List>
        </Content>
        <View style={{ flex: 2, padding: 15}}>
            <Text style={{ fontSize: 20, margin: 15, fontFamily: 'GoogleSans-Regular', textAlign: 'center' }} >Pilihan Anda</Text>
            <Image
              source={{ uri: imageKandidat }}
              style={{ width: 150, height: 150, borderRadius: 150 /2, alignSelf:'center'}}
            />
            <Text style={{ fontSize: 16, margin: 15, fontFamily: 'GoogleSans-Regular', textAlign: 'center' }} >{namaKandidat}</Text>
          </View>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  headerContainer: {
    height: 140,
  },
  headerText: {
    color: '#fff8f8',
  },
  screenContainer: {
    paddingTop: 8,
    width: '100%',
  },
  screenStyle: {
    height: 33,
    marginTop: 2,
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%'
  },
  screenTextStyle: {
    fontSize: 14,
    marginLeft: 10
  },
  selectedTextStyle: {
    fontWeight: 'bold',
    color: '#00adff'
  },
  activeBackgroundColor: {
    backgroundColor: '#e3e3e3'
  },
});

export default drawerContentComponents;