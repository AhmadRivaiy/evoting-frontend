import React from 'react';
import { View, Animated, Image, ScrollView, Text, Dimensions, StyleSheet, ImageBackground, StatusBar, Platform} from "react-native";
import { Container, Content, Card, CardItem, Footer, FooterTab, Button, Body, Accordion, Icon  } from 'native-base';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import Entypo from 'react-native-vector-icons/Entypo';
import YouTube from 'react-native-youtube';

const HEADER_EXPANDED_HEIGHT = 350;
const HEADER_COLLAPSED_HEIGHT = 60;
const { width: SCREEN_WIDTH } = Dimensions.get("screen")
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 75 : 60;
const PAD_T = getStatusBarHeight() - 20;

class DetailKandidat extends React.Component {
    constructor(props){
        super(props);
        const { params } = this.props.navigation.state;
        this.state = {
            //Bila Sudah Login
            //set isLogined true pada asyncstorage
            //Saat sudah terload baru pindah ke Dashboard
            //Data di inputkan ke AsyncStorage
            //Agar aplikasi berjalan smooth
            idKandidat: params.userId,
            namaKandidat: params.nama,
            imageKandidat: params.image,
            scrollY: new Animated.Value(0),
            playing: true,
            setPlaying: true,
            userRef: null,
            playYt: false
        }
    }

    componentDidMount(){
        this.setState({ playYt: true })
    }

    componentWillUnmount(){
        this.setState({ playYt: false })
    }

    _renderHeader(item, expanded) {
        return (
          <View style={{
            flexDirection: "row",
            padding: 20,
            justifyContent: "space-between",
            alignItems: "center" ,
            backgroundColor: "#3a91aa", }}>
          <Text style={{ fontWeight: 'bold', color: '#fff', fontSize: 15 }}>
              {" "}{item.title}
            </Text>
            {expanded
              ? <Entypo style={{ fontSize: 25, color: 'white' }} name="chevron-up" />
              : <Entypo style={{ fontSize: 25, color: 'white' }} name="chevron-down" />}
          </View>
        );
      }
      _renderContent(item) {
        return (
          <Text
            style={{
              backgroundColor: "#e7e7e7",
              padding: 20,
            }}
          >
            {item.content}
          </Text>
        );
      }

    render() {
        const headerHeight = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_EXPANDED_HEIGHT-HEADER_COLLAPSED_HEIGHT],
            outputRange: [HEADER_EXPANDED_HEIGHT, HEADER_COLLAPSED_HEIGHT],
            extrapolate: 'clamp'
        });
        
        const headerTitleOpacity = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_EXPANDED_HEIGHT-HEADER_COLLAPSED_HEIGHT],
            outputRange: [0, 1],
            extrapolate: 'clamp'
        });
        
          const heroTitleOpacity = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_EXPANDED_HEIGHT-HEADER_COLLAPSED_HEIGHT],
            outputRange: [1, 0],
            extrapolate: 'clamp'
        });
      
        const headerTitle = this.state.namaKandidat;
        
        const dataArray = [
            { title: "VISI", content: "Lorem ipsum dolor sit amet" },
            { title: "MISI", content: "Lorem ipsum dolor sit amet" },
            { title: "BIODATA", content: "Lorem ipsum dolor sit amet" },
        ];

        const { playYt } = this.state;
        
        return (
            <Container>
                <StatusBar barStyle="light-content"/>
                <Animated.View style={[styles.header, { height: headerHeight, shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 12,
                        },
                        shadowOpacity: 0.58,
                        shadowRadius: 13.00,
                        elevation: 24 }]}>
                    <ImageBackground
                        source={{ uri: this.state.imageKandidat }}
                        style={{ flex: 1 }}
                        resizeMode={"cover"}
                        
                    >
                    <Animated.View style={{position: 'absolute', bottom: 16, left: 16, opacity: heroTitleOpacity}}>
                        {/* <Text style={{ textAlign: 'center', fontSize: 32, color: 'white', }}>{headerTitle}</Text> */}
                    </Animated.View>
                    <Animated.View style={{ backgroundColor:'#3a91aa', opacity: headerTitleOpacity}}>
                        <View style={{ height: 80 + PAD_T, flexDirection: 'column', alignItems: 'center', justifyContent: 'flex-end' }}>
                            <Text style={{ margin: 17, fontFamily: 'ProductSans-Bold', fontSize: 20, color: '#fff' }}>{this.state.namaKandidat.length >= 15 ? this.state.namaKandidat.slice(0,15)+'...' : this.state.namaKandidat}</Text>
                        </View>
                    </Animated.View>
                    </ImageBackground>
                </Animated.View>
                
                <ScrollView
                    contentContainerStyle={styles.scrollContainer}
                    onScroll={Animated.event(
                        [{ nativeEvent: {
                            contentOffset: {
                            y: this.state.scrollY
                            }
                        }
                        }],
                        {
                            useNativeDriver: false
                        }
                        )
                    }
                    scrollEventThrottle={16}
                >
                    <View
                        style={{
                            flex: 1,
                            backgroundColor:'pink',
                            borderRadius: 10,
                            marginTop:15,
                            alignItems: 'center',
                            marginBottom: 20
                        }}
                    >
                        <YouTube
                            videoId="KVZ-P-ZI6W4" // The YouTube video ID
                            apiKey="AIzaSyA_5gQ4B-mzYF9-m_UWC6cIguccwsdB-Kg"
                            play={playYt} // control playback of video with true/false
                            //fullscreen // control whether the video should play in fullscreen or inline
                            loop // control whether the video should loop when ended
                            style={{ alignSelf: 'stretch', height: 300, borderRadius: 10 }}
                            />
                    </View>

                    {/* Visi */}
                    <Accordion
                        dataArray={dataArray}
                        animation={true}
                        expanded={true}
                        renderHeader={this._renderHeader}
                        renderContent={this._renderContent}
                    />
                </ScrollView>
                
                <Footer>
                    <FooterTab>
                        <Button
                            full
                            style={{ backgroundColor: '#009991' }}
                            onPress={() => this.props.navigation.navigate('Vote', {userId: this.state.idKandidat, nama: this.state.namaKandidat, image: this.state.imageKandidat})}
                        >
                            <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 16 }}>VOTE</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#eaeaea',
    },
    scrollContainer: {
      padding: 16,
      paddingTop: HEADER_EXPANDED_HEIGHT
    },
    header: {
      backgroundColor: 'lightblue',
      position: 'absolute',
      width: SCREEN_WIDTH,
      top: 0,
      left: 0,
      zIndex: 9999
    },
    title: {
      marginVertical: 16,
      color: "black",
      fontWeight: "bold",
      fontSize: 24
    }
  });

export default DetailKandidat