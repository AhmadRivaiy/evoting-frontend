import React from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import { StyleSheet, View, Text, Dimensions, Image, StatusBar } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { RoutingHelper } from '../../helpers/RoutingHelper';
import AsyncStorage from '@react-native-community/async-storage';

const styles = StyleSheet.create({
    content: {
        backgroundColor:'#fff',
        borderRadius: 5,
        width: Dimensions.get('window').width/1.2,
        height: Dimensions.get('window').height/1.5,
        padding: 15,
        marginTop: 20,
        alignSelf: 'center',
        alignItems: 'center'
    },
    buttonCircle: {
        width: 40,
        height: 40,
        backgroundColor: 'rgba(0, 0, 0, .2)',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    doneButton: {
        width: 100,
        height: 40,
        backgroundColor: 'rgba(0, 0, 0, .2)',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        fontSize: 16,
        color: '#0f9797',
        fontWeight: 'bold',
        marginBottom: 10
    },
    welcomeLogo: {
        width: 200,
        height: 200,
        marginBottom: 15,
    },
    logoIntro: {
        color: '#fff',
        fontFamily: 'ProductSans-Bold',
        fontSize: 35,
        fontWeight: 'bold',
        alignSelf: 'center'
    },
    deskripsi: {
        fontSize: 14
    }
  //[...]
});

export default class WelcomeScreen extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            slides: []
        }
    }

    async componentDidMount(){
      try {
        var value = await AsyncStorage.getItem('currentLogin')
        if(value !== null) {
          var nextVal = JSON.parse(value.toString());
          this.setState({
            slides: nextVal
          })
        }
      } catch(e) {
        console.log(e)
      }
    }

  _renderItem = ({ item, key }) => {
    return (
        <View style={styles.content} key={key}>
            <Image
              source={{ uri: item.image }}
              style={item.image == null ? '' : styles.welcomeLogo}
            />
            <Text style={styles.title}>{item.title}</Text>
            <Text style={styles.deskripsi}>{item.deskripsi}</Text>
            <View
                style={{ flex: 1, justifyContent: 'flex-end'}}
            >
                <Text style={{ textAlign: 'center', alignSelf: 'stretch'}}>{item.footer}</Text>
            </View>
        </View>
    );
  }
  _renderNextButton = () => {
    return (
      <View>
        
      </View>
    );
  };
  _renderDoneButton = () => {
    return (
      <TouchableOpacity 
        onPress={() => this.props.navigation.dispatch(RoutingHelper.finishActivity('Login'))}
      >
        <View style={styles.doneButton}>
          <Text style={{ color: "rgba(255, 255, 255, .9)" }}>Mulai</Text>
        </View>
      </TouchableOpacity>
    );
  };
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#0f9797', paddingTop: 50 }}><Text style={styles.logoIntro}>E - VOTING</Text>
      <StatusBar backgroundColor="#0f9797" barStyle="light-content" />
        <AppIntroSlider
            keyExtractor={(item, index) => index.toString()}
            data={this.state.slides}
            renderItem={this._renderItem}
            renderDoneButton={this._renderDoneButton}
            renderNextButton={this._renderNextButton}
        />
      </View>
    );
  }
}