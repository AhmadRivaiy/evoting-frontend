import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
    StatusBar,
    SafeAreaView,
    TouchableOpacity,
    TextInput,
    ActivityIndicator
} from 'react-native';

import {
    Item,
    Input,
    Form,
    Label,
    Button,
    Text,
    Thumbnail
} from 'native-base';
import Logo from '../../assets/images/logo_evote.png';
import { RoutingHelper } from '../../helpers/RoutingHelper';
import { authService } from '../../services/Auth.service';
import AwesomeAlert from 'react-native-awesome-alerts';
import { Grid, Row, Col } from 'react-native-easy-grid';
import DeviceInfo from 'react-native-device-info'; 
import AsyncStorage from '@react-native-community/async-storage';
import firebase from '@react-native-firebase/auth'
import oAuthFirebase from '@react-native-firebase/app'

const uniqueId = DeviceInfo.getUniqueId();
class LoginScreen extends React.Component {
    constructor(props){
        super(props);
        this._isMounted = false;
        this.state = {
            showAlert: false,
            no_tlp: '',
            messageAlert: '',
            titleAlert: '',
            showAlertSucces: false,
            showLoading: false,
            loadVerfy: false,
            isLogined: false,

            id: '',
            nama: '',
            email: '',
            no_tlp: '',

            phone: '',
            confirmResult: null,
            verificationCode: '',
            userId: '',
            user: '',
            isAuth: false,
            isVoted: '0',
            deviceIsSame: false,
            deviceID: ''
        }
    }

    validatePhoneNumber = () => {
        var regexp = /^\+[0-9]?()[0-9](\s|\S)(\d[0-9]{8,16})$/
        return regexp.test(this.state.phone)
    }

    componentDidMount() {
        // Bind the variable to the instance of the class.
        this._isMounted = true;
        this._isMounted && this.authListener();
    }

    async authListener() {
        try{
            await oAuthFirebase.auth().onAuthStateChanged((user) => {
                if (user !== null) {
                    //this.handlePostLogin();
                    AsyncStorage.setItem('isLogined', 'true')
                    AsyncStorage.setItem('isFirebase', 'true')
                    this.setState({ isLogined: true })
                    this.props.navigation.dispatch(RoutingHelper.finishActivity('Dashboard'))
                }else{
                    this.setState({ isLogined: false })
                }
            });
        }catch(error){
            //Promise.reject('Tes');
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
     }

    handleSendCode = () => {
        // Request to send OTP
        if (this.validatePhoneNumber()) {
        this.setState({ loadVerfy: true, showAlertSucces: false})
          firebase()
            .signInWithPhoneNumber(this.state.phone)
            .then(confirmResult => {
              this.setState({ confirmResult, loadVerfy: false })
            })
            .catch(error => {
                alert(error.message)
                console.log('error')
            })
        } else {
          alert('Invalid Phone Number')
        }
    }

    changePhoneNumber = () => {
        this.setState({ confirmResult: null, verificationCode: '' })
    }
    
    handleVerifyCode = () => {
        // Request for OTP verification
        this.setState({ loadVerfy: true })
        const { confirmResult, verificationCode } = this.state
        try {
            if (verificationCode.length == 6) {
                confirmResult
                  .confirm(verificationCode)
                  .then(user => {
                    this.handlePostLogin()
                  })
                  .catch(error => {
                    this.setState({ showAlert: true, titleAlert: 'Error', messageAlert: error.message, loadVerfy: false, showAlertSucces: false })
                  })
              } else {
                alert('Please enter a 6 digit OTP code.')
            }
        }catch (error) {
            alert('error')
        }
    }

    async handleLogout() {
        if(this.state.isLogined){
            await oAuthFirebase.auth().signOut();
        }
        await AsyncStorage.clear();
        this.setState({ showAlert: false })
        new Promise(() => {
            setTimeout( () => {
                this.props.navigation.dispatch(RoutingHelper.finishActivity('Splash'))
            }, 1000);
        })
      }

    //= =   =   =   =   =   =   =   =   =   =   =   =   =   =   =   =   =   =   =   =   =   =   =   =   =   =   =   =   =   =
    async handleLogin() {
        try{
            if (this.validatePhoneNumber()) {
                await authService.AuthUser(this.state.phone)
                .then(
                    user => {
                        AsyncStorage.setItem('currentUser', JSON.stringify(user.data))
                        AsyncStorage.setItem('currentToken', JSON.stringify(user.accessToken))
                        this.setState({
                            showAlertSucces: true,
                            messageAlert: 'Nama : ' + user.data.payload.nama + '\nEmail : ' + user.data.payload.email,
                            titleAlert: 'Verifikasi',
                            id: user.data.payload.id,
                            nama: user.data.payload.nama,
                            email: user.data.payload.email,
                            isVoted: user.data.payload.is_voted,
                            deviceID: user.data.device_id
                        })
                },
                    error => {
                        this.setState({ showAlert: true, messageAlert: error, titleAlert: 'Error' })
                })
                .catch((error) => {
                    this.setState({ showAlert: true, titleAlert: 'Error', messageAlert: 'Tidak Ada Koneksi Internet!' })
                });
            }else{
                this.setState({ showAlert: true, messageAlert: 'Nomor Telepon Salah', titleAlert: 'Error' })
            }
        }catch(e){
            this.setState({ showAlertSucces: false, showAlert: true, messageAlert: 'Hubungi Tikomdik!\nKode Error : 0021', titleAlert: 'Error'})
        }
    }
    
    async handlePostLogin() {
        var dataLogin = {
            id: this.state.id,
            unique_id: uniqueId
        }
        try{
            await authService.postLogin(dataLogin)
            .then(
                user => {
                    //this.handleSendCode()
                    this.setState({ loadVerfy: false })
                    AsyncStorage.setItem('isLogined', 'true')
                    this.props.navigation.dispatch(RoutingHelper.finishActivity('Dashboard'))
            },
                error => {
                    if(this.state.isVoted == '1'){
                        this.setState({ loadVerfy: false })
                        AsyncStorage.setItem('isLogined', 'true')
                        this.props.navigation.dispatch(RoutingHelper.finishActivity('Dashboard'))
                    }else if(this.state.deviceIsSame){
                        this.setState({ loadVerfy: false })
                        AsyncStorage.setItem('isLogined', 'true')
                        this.props.navigation.dispatch(RoutingHelper.finishActivity('Dashboard'))
                    }else{
                        this.setState({ showAlertSucces: false, showAlert: true, messageAlert: 'Anda Tidak Diperbolehkan Diperangkat Ini!', titleAlert: 'Error'})
                    }
            })
            .catch((error) => {
                console.log('Tes 3')
                this.setState({ showAlertSucces: false, showAlert: true, messageAlert: 'Tidak Ada Koneksi Internet!', titleAlert: 'Error'})
            });
        }catch(error){
            console.log('Tes 4')
            this.setState({ showAlertSucces: false, showAlert: true, messageAlert: 'Hubungi Tikomdik!\nKode Error : 0022', titleAlert: 'Error'})
        }
    }

    async handleConflict(){
        var dataLogin = {
            id: uniqueId,
            no_tlp: this.state.phone
        }
        try{
            await authService.checkingVoted(dataLogin)
            .then(
                user => {
                    this.setState({ loadVerfy: false, showAlertSucces: false})
                    if(user.data.status == 1 && user.data.device_id == uniqueId){
                        this.setState({ deviceIsSame : true})
                        this.props.navigation.dispatch(RoutingHelper.finishActivity('Dashboard'))
                    }else if(user.data.status == 0){
                        if(this.state.deviceID == null){
                            this.setState({ loadVerfy: false, showLoading: false })
                            //this.handleSendCode();
                        }else if(this.state.deviceID == uniqueId){
                            this.setState({ deviceIsSame : true})
                            this.props.navigation.dispatch(RoutingHelper.finishActivity('Dashboard'))
                        }else{
                            new Promise(() => {
                                setTimeout( () => {
                                    this.setState({
                                        showAlert: true, messageAlert: 'Anda Tidak Diperbolehkan!', titleAlert: 'Error'
                                    })
                                }, 1000);
                            })
                        }
                    }
            },
                error => {
                    this.setState({ showAlertSucces: false })
                    if(this.state.deviceID == null){
                        this.handleSendCode();
                    }else if(this.state.deviceID == uniqueId){
                        new Promise(() => {
                            setTimeout( () => {
                                AsyncStorage.setItem('isLogined', 'true')
                                this.props.navigation.dispatch(RoutingHelper.finishActivity('Dashboard'))
                            }, 500);
                        })
                    }else{
                        new Promise(() => {
                            setTimeout( () => {
                                this.setState({
                                    showAlert: true, messageAlert: error, titleAlert: 'Error'
                                })
                            }, 1000);
                        })
                    }
                    // new Promise(() => {
                    //     setTimeout( () => {
                    //         this.setState({
                    //             showAlert: true, messageAlert: 'Terjadi Error\nKode : 0021', titleAlert: 'Error'
                    //         })
                    //     }, 500);
                    // })
            })
            .catch((error) => {
                console.log(error)
                this.setState({ showAlertSucces: false, showAlert: true, messageAlert: 'Tidak Ada Koneksi Internet!', titleAlert: 'Error'})
            });
        }catch(error){
            console.log('Tes 23')
            this.setState({ showAlertSucces: false, showAlert: true, messageAlert: 'Hubungi Tikomdik!\nKode Error : 0023', titleAlert: 'Error'})
        }
    }

    renderConfirmationCodeView = () => {
        return (
          <View style={styles.verificationView}>
            <TextInput
              style={styles.textInput}
              placeholder='Verification code'
              placeholderTextColor='gray'
              value={this.state.verificationCode}
              keyboardType='numeric'
              onChangeText={verificationCode => {
                this.setState({ verificationCode })
              }}
              maxLength={6}
            />
            <TouchableOpacity
              style={[styles.themeButton, { marginTop: 20 }]}
              onPress={this.handleVerifyCode}>
              <Text style={styles.themeButtonTitle}>Verify Code</Text>
            </TouchableOpacity>
          </View>
        )
    }

    render() {
        const {showAlertSucces, showAlert, messageAlert, titleAlert, showLoading} = this.state;
        return (
            // <View style={styles.container}>
            //     <StatusBar backgroundColor="#127863" barStyle="light-content" />
            //     {/* <Image style={styles.bgImageStyle} source={BgImage} /> */}
                // <View style={styles.logoStyle}>
                //     <Thumbnail circle larget source={Logo}/>
                //     <Text style={styles.textLogoStyle}></Text>
                // </View>
            //     <Form style={styles.formLoginStyle}>
            //         <Item floatingLabel>
            //             <Label style={styles.labelNomor}>Nomor Handphone</Label>
            //             <Input
            //                 style={styles.inputStyle}
            //                 keyboardType="numeric"
            //                 onChangeText={(value) => this.setState({no_tlp: value})}
            //                 value={this.state.no_tlp}
            //                 />
            //         </Item>
            //         <Button block style={styles.buttonStyle} onPress={() => this.handleLogin()}>
            //             <Text>Masuk</Text>
            //         </Button>
            //     </Form>

                // <AwesomeAlert
                //     show={showAlert}
                //     title={titleAlert}
                //     message={messageAlert}
                //     closeOnTouchOutside={true}
                //     closeOnHardwareBackPress={false}
                //     showConfirmButton={true}
                //     confirmText="Ok"
                //     confirmButtonColor="#DD6B55"
                //     onConfirmPressed={() => {
                //         this.setState({ showAlert: false, no_tlp: '' })
                //     }}
                // />

                // <AwesomeAlert
                //     show={showAlertSucces}
                //     title={titleAlert}
                //     showProgress={showLoading}
                //     progressColor="black"
                //     message={messageAlert}
                //     closeOnTouchOutside={true}
                //     closeOnHardwareBackPress={false}
                //     showConfirmButton={true}
                //     confirmText="Ok"
                //     showCancelButton={true}
                //     cancelText="Bukan Saya"
                //     confirmButtonColor="#00b248"
                //     onCancelPressed={() => this.setState({ showAlertSucces: false })}
                //     onConfirmPressed={() => this.handleVerification()}
                // />
            //     {/* this.props.navigation.dispatch(RoutingHelper.finishActivity('Dashboard')) & this.setState({ showAlertSucces: false }) */}
            // </View>
            <View style={[styles.container, { backgroundColor: '#fafafa' }]}>
                <View style={styles.page}>
                <View style={styles.logoStyle}>
                    <Thumbnail circle large source={Logo}/>
                    <Text style={styles.textLogoStyle}>EVote-FOJB</Text>
                </View>
                <TextInput
                    style={styles.textInput}
                    placeholder='No. Hp dengan Kode Negara (+62...)'
                    placeholderTextColor='gray'
                    keyboardType='phone-pad'
                    value={this.state.phone}
                    onChangeText={phone => {
                    this.setState({ phone })
                    }}
                    maxLength={15}
                    editable={this.state.confirmResult ? false : true}
                />

                <TouchableOpacity
                    style={[styles.themeButton, { marginTop: 20 }]}
                    onPress={this.state.confirmResult
                        ? this.changePhoneNumber
                        : () => this.handleLogin()
                    }>
                    <Text style={styles.themeButtonTitle}>
                        {this.state.confirmResult ? 'Change Phone Number' : 'Send Code'}
                    </Text>
                </TouchableOpacity>
                
                <ActivityIndicator size="large" animating={this.state.loadVerfy} color="blue" style={{ margin: 25}}/>
                {this.state.confirmResult ? this.renderConfirmationCodeView() : null}
                </View>

                <AwesomeAlert
                    show={showAlertSucces}
                    title={titleAlert}
                    showProgress={showLoading}
                    progressColor="black"
                    message={messageAlert}
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={false}
                    showConfirmButton={true}
                    confirmText="Ok"
                    showCancelButton={true}
                    cancelText="Bukan Saya"
                    confirmButtonColor="#00b248"
                    onCancelPressed={() => this.setState({ showAlertSucces: false })}
                    onConfirmPressed={() => this.handleConflict()}
                />

                <AwesomeAlert
                    show={showAlert}
                    title={titleAlert}
                    message={messageAlert}
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={false}
                    showConfirmButton={true}
                    confirmText="Ok"
                    confirmButtonColor="#DD6B55"
                    onConfirmPressed={() => this.handleLogout()}
                />
            </View>
        );
    }
}

// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         backgroundColor: '#fafafa'
//     },
//     bgImageStyle: {
//         flex: 1,
//         resizeMode: 'cover',
//         justifyContent: 'center',
//         alignItems: 'center',
//         position: 'absolute',
//         width: Dimensions.get("window").width,
//         height: Dimensions.get("window").height
//     },
    // logoStyle: {
    //     marginTop: 200,
    //     marginBottom: 80,
    //     alignItems: 'center',
    //     justifyContent: 'center'
    // },
    // textLogoStyle: {
    //     fontSize: 15,
    //     color: 'white'
    // },
//     formLoginStyle: {
//         marginTop: -30,
//         paddingLeft: 10,
//         paddingRight: 30
//     },
//     labelNomor: {
//         color: '#009991',
//         fontSize: 14
//     },
//     inputStyle: {
//         color: '#009991',
//         marginBottom: 6,
//         fontSize: 14
//     },
//     buttonStyle: {
//         backgroundColor: '#009991',
//         marginTop: 26,
//         paddingTop: 10,
//         marginLeft: 16,
//         marginRight: 16
//     }
// });

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#aaa'
    },
    logoStyle: {
        marginBottom: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textLogoStyle: {
        fontSize: 18,
        color: '#0f9797',
    },
    page: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
    },
    textInput: {
      marginTop: 20,
      width: '90%',
      height: 40,
      borderColor: '#555',
      borderWidth: 2,
      borderRadius: 5,
      paddingLeft: 10,
      color: '#000',
      fontSize: 16
    },
    themeButton: {
      width: '90%',
      height: 50,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#0f9797',
      borderColor: '#555',
      borderWidth: 2,
      borderRadius: 5
    },
    themeButtonTitle: {
      fontSize: 24,
      fontWeight: 'bold',
      color: '#fff'
    },
    verificationView: {
      width: '100%',
      alignItems: 'center',
      marginTop: 10
    }
  })

export default LoginScreen