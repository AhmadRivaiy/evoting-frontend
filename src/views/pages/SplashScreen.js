import React from 'react';
import { StyleSheet, View, Image, Animated, ActivityIndicator, StatusBar } from 'react-native';
import { Button, Container, Text } from 'native-base';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Logo from '../../assets/images/logo_evote.png';
import { RoutingHelper } from '../../helpers/RoutingHelper';
import { HttpService } from '../../services/Http.service';
import { apiWelcome } from '../../helpers/ApiStore';
import AsyncStorage from '@react-native-community/async-storage';
import AwesomeAlert from 'react-native-awesome-alerts';

class SplashScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            LogoAnime: new Animated.Value(0),
            LogoText: new Animated.Value(0),
            loadingSpinner: false,
            showAlert: false,
            titleAlert: '',
            messageAlert: ''
            //Bila Sudah Login
            //Lakukan Load Data seperti Profile Disini
            //Saat sudah terload baru pindah ke Dashboard
            //Data di inputkan ke AsyncStorage
            //Agar aplikasi berjalan smooth

            //Adanya Maintenance dan Cek Versi Aplikasi Hooks pada SplashScreen
        }
    }

    async componentDidMount() {
        try {
            const value = await AsyncStorage.getItem('isLogined')
            if(value !== null) {
                // await axios -> getDataPemilih, getDataKandidat (method get)
                // .then(data => {
                //     //setData AsyncStorage
                // })
                this._toDashboard();
            }else{
                try{
                    await HttpService.getData(apiWelcome)
                    .then(data => {
                        AsyncStorage.setItem('currentLogin', JSON.stringify(data.data.list_w))
                        AsyncStorage.setItem('currentCandidate', JSON.stringify(data.data.list_candidate))
                        setTimeout( () => {
                            Animated.timing(this.state.LogoAnime).stop();
                        }, 3000);
                    }, error => {
                        new Promise(() => {
                            setTimeout( () => {
                                this.setState({
                                    showAlert: true,
                                    titleAlert: 'Erorr!',
                                    messageAlert: error
                                })
                            }, 2000);
                        })
                    })
                    .catch(error => {
                        console.log(error)
                    })
                    this._toWelcome();
                }catch(err){
                    console.log(err)
                }
            }
        }catch(e) {
            console.log('error reading')
        }
    }

    _toDashboard(){
        Animated.parallel([
            Animated.spring(this.state.LogoAnime, {
                toValue: 1,
                tension: 10,
                friction: 3,
                duration: 1000,
                useNativeDriver: false // Add This line
            }).start(() => {
                this.props.navigation.dispatch(RoutingHelper.finishActivity('Dashboard'))
            }),

            Animated.timing(this.state.LogoText, {
                toValue: 1,
                duration: 1700,
                useNativeDriver: true // Add This line
            }),
        ]).start();
    }

    _toWelcome(){
        Animated.parallel([
            Animated.loop(
                Animated.spring(this.state.LogoAnime, {
                    toValue: 1,
                    tension: 5,
                    friction: 3,
                    duration: 1000,
                    useNativeDriver: false // Add This line
                }),{
                    iterations: -1
                }).start(() => {
                    this.props.navigation.dispatch(RoutingHelper.finishActivity('Welcome'))
                }),

            Animated.timing(this.state.LogoText, {
                toValue: 1,
                duration: 1700,
                useNativeDriver: true // Add This line
            }),
        ]).start();
    }

    componentWillUnmount() {
        //Clear Routes Splash
        Animated.timing(this.state.LogoAnime).stop();
    }

    render() {
        const { showAlert, titleAlert, messageAlert } = this.state;

        return (
            <View style={styles.container}>
            <StatusBar backgroundColor="#fafafa" barStyle="dark-content" />
                <Animated.View style={{
                    opacity: this.state.LogoAnime,
                    top: this.state.LogoAnime.interpolate({
                        inputRange: [0, 1],
                        outputRange: [80, 0]
                    })
                }}>
                    <Image source={Logo} style={{ width: 170, height: 170}}/>
                </Animated.View>

                <Animated.View style={{
                    opacity: this.state.LogoText
                }}>
                    <Text style={styles.logoText}>Voting App</Text>
                </Animated.View>

                <AwesomeAlert
                    show={showAlert}
                    title={titleAlert}
                    message={messageAlert}
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={false}
                    showConfirmButton={true}
                    confirmText="Ok"
                    confirmButtonColor="#DD6B55"
                    onConfirmPressed={() => this.setState({ showAlert: false })}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fafafa',
        justifyContent: 'center',
        alignItems: 'center'
    },

    logoText: {
        color: '#009991',
        fontFamily: 'ProductSans-Bold',
        fontSize: 30,
        marginTop: 29.1,
        fontWeight: '300'
    }
})

export default SplashScreen