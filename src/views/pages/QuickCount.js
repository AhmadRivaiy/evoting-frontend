import React from 'react';
import { StyleSheet, Text, View, BackHandler, StatusBar, Dimensions } from 'react-native';
import { Button, Card, CardItem, Body, Container, Content } from 'native-base';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph,
    StackedBarChart
  } from "react-native-chart-kit";

class QuickCount extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            //Bila Sudah Login
            //set isLogined true pada asyncstorage
            //Saat sudah terload baru pindah ke Dashboard
            //Data di inputkan ke AsyncStorage
            //Agar aplikasi berjalan smooth
        }
    }

    render() {
        const dataRing = {
            labels: ["Budi", "Ani", "Lucas"], // optional
            data: [0.4, 0.6, 0.8]
        };

        const dataBar = {
            labels: ["Budi", "Ani", "Lucas"],
            datasets: [
              {
                data: [20, 45, 28]
              }
            ]
          };
        
        const chartConfig = {
            backgroundGradientFrom: "#fff",
            backgroundGradientFromOpacity: 0,
            backgroundGradientTo: "#fff",
            backgroundGradientToOpacity: 0,
            color: (opacity = 1) => `rgba(7,75,75, ${opacity})`,
            strokeWidth: 5, // optional, default 3
            barPercentage: 1 // optional
        };

        const screenWidth = Dimensions.get('screen').width/1.1;
        const screenHeight = Dimensions.get('screen').height/4.1;
        return (
            <Container style={{padding: 20, backgroundColor: '#3a91aa'}}>
            <StatusBar barStyle="light-content"/>
                <View
                    style={{
                        backgroundColor:'lightblue',
                        borderRadius: 10,
                        marginTop: getStatusBarHeight() + 40,
                        marginBottom: 20,
                        padding: 15
                    }}
                >
                    <Text style={{ color: 'white' }}>Data Terakhir Terambil</Text>
                </View>
                <View
                    style={{
                        flex: 1,
                        backgroundColor:'#cfeaea',
                        borderRadius: 10,
                        marginTop: 15,
                        alignItems: 'center',
                        marginBottom: 10
                    }}
                >
                    <ProgressChart
                        data={dataRing}
                        width={screenWidth}
                        height={230}
                        strokeWidth={16}
                        radius={32}
                        chartConfig={chartConfig}
                        hideLegend={false}
                    />
                </View>
                <View
                    style={{
                        flex: 1,
                        backgroundColor:'#cfeaea',
                        borderRadius: 10,
                        marginTop:15,
                        alignItems: 'center',
                        marginBottom: 20
                    }}
                >
                    <BarChart
                        //style={graphStyle}
                        data={dataBar}
                        width={screenWidth}
                        height={220}
                        yAxisSuffix="%"
                        chartConfig={chartConfig}
                    />
                </View>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    centered: {
        alignItems: 'center',
        justifyContent: 'center',
    }
});

export default QuickCount