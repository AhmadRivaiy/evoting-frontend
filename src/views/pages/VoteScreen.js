import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, StatusBar, Dimensions, Image, ActivityIndicator, ImageBackground } from 'react-native';
import { Container, Button } from 'native-base';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Modal from 'react-native-modal';
import AsyncStorage from '@react-native-community/async-storage';
import { RoutingHelper } from '../../helpers/RoutingHelper';
import axios from 'axios';
import { apiPostVote } from '../../helpers/ApiStore';
import { handleGetItem } from '../../components/storeAsync.storage'
import bgVote from '../../assets/images/bgVote.png';
import bgDrawer from '../../assets/images/bgDrawer2.png';

class VoteScreen extends React.Component {
    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
        this._isMounted = false;
        this.state = {
            //Bila Sudah Login
            //set isLogined true pada asyncstorage
            //Saat sudah terload baru pindah ke Dashboard
            //Data di inputkan ke AsyncStorage
            //Agar aplikasi berjalan smooth
            idKandidat: params.userId,
            namaKandidat: params.nama,
            imageKandidat: params.image,
            nama: '',
            userId: null,
            condIsVoted: null,
            modalConfirm: false,
            modalError: false,
            messageError: '',
            btnVote: false,
            loadPostVote: false,
            tokenUser: ''
        }
    }

    componentDidMount() {
        this._isMounted = true;
        this._isMounted && this.getDataIzin();
    }

    async getDataIzin() {
        try {
            await handleGetItem('currentUser')
                .then(x => {
                    if (x.is_voted == 1) {
                        this.setState({
                            condIsVoted: true
                        })
                    } else {
                        this.setState({
                            condIsVoted: false
                        })
                    }
                }).catch(err => {
                    console.log(err)
                })

            await handleGetItem('currentToken')
                .then(x => {
                    this.setState({
                        tokenUser: x.tokenIs
                    })
                }).catch(err => {
                    console.log(err)
                })
        } catch (e) {
            console.log(e)
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
        this.setState({ modalConfirm: false })
    }

    async handleVote() {
        this.setState({ btnVote: true, loadPostVote: true })
        var dataCandidate = {
            idCandidate: this.state.idKandidat
        }

        const headers = {
            'Content-Type': 'application/json',
            'x-access-token': 'token=' + this.state.tokenUser
        }

        try {
            await axios.post(apiPostVote, dataCandidate, {
                headers: headers
            })
                .then((response) => {
                    AsyncStorage.setItem('currentUser', JSON.stringify(response.data.data))
                    AsyncStorage.setItem('currentCandidateVoted', JSON.stringify(response.data.data_kandidat))
                    this.getDataIzin();
                    this.setState({ loadPostVote: false, condIsVoted: false, modalConfirm: false, modalError: true, messageError: response.data.message })
                })
                .catch((error) => {
                    this.setState({ loadPostVote: false, condIsVoted: false, modalConfirm: false, modalError: true, messageError: error.response.data.message })
                })
        } catch (error) {
            console.log(error)
        }
    }

    handleCancel() {
        this.setState({ modalConfirm: false })
        new Promise(() => {
            setTimeout(() => {
                this.props.navigation.dispatch(RoutingHelper.finishActivity('Dashboard'))
            }, 600);
        })
    }

    handleSuccessVote(){
        this.setState({ modalError: false, messageError: '' })
        new Promise(() => {
            setTimeout(() => {
                this.props.navigation.dispatch(RoutingHelper.finishActivity('Dashboard'))
            }, 600);
        })
    }

    render() {
        const { modalConfirm, messageError, modalError, btnVote, loadPostVote } = this.state;
        return (
            <Container>
                <StatusBar barStyle="dark-content" />
                <ImageBackground
                source={bgVote}
                    style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        width: Dimensions.get('screen').width
                    }}
                >
                    <Image
                        source={{ uri: this.state.imageKandidat }}
                        style={{ width: 250, height: 250, borderRadius: 250 / 2, marginBottom: 50 }}
                    />
                    <Text style={{ fontFamily: 'ProductSans-Bold', fontSize: 24 }}>
                        {this.state.namaKandidat}
                    </Text>
                    <Button
                        block
                        primary
                        disabled={this.state.condIsVoted}
                        style={{ margin: 20 }}
                        onPress={() => this.setState({ modalConfirm: true })}>
                        <Text style={{ fontWeight: 'bold', color: 'white' }}>VOTE</Text>
                    </Button>
                </ImageBackground>

                <View>
                    <Modal
                        isVisible={modalConfirm}
                        onBackdropPress={() => this.setState({ modalConfirm: false })}
                        backdropOpacity={0.8}
                        animationIn="zoomInDown"
                        animationOut="zoomOutUp"
                        animationInTiming={2000}
                        animationOutTiming={1000}
                        backdropTransitionInTiming={600}
                        backdropTransitionOutTiming={600}
                    >
                        <View style={{
                            backgroundColor: 'white',
                            borderRadius: 10,
                            padding: 15
                        }}>
                            <Text
                                style={{
                                    alignSelf: 'center',
                                    textAlign: 'center',
                                    margin: 10
                                }}
                            >
                                Anda Akan Memilih :
                            </Text>
                            <Image
                                source={{ uri: this.state.imageKandidat }}
                                style={{ width: 150, height: 150, borderRadius: 150 / 2, alignSelf: 'center' }}
                            />
                            <Text
                                style={{
                                    alignSelf: 'center',
                                    textAlign: 'center',
                                    margin: 10,
                                    fontFamily: 'ProductSans-Bold',
                                    fontSize: 24,
                                    marginBottom: 15
                                }}
                            >
                                {this.state.namaKandidat}
                            </Text>
                            <Text
                                style={{
                                    alignSelf: 'center',
                                    textAlign: 'center',
                                    margin: 10
                                }}
                            >
                                Apakah anda yakin, memberi hak suara Anda kepada Calon ini?
                            </Text>
                            <ActivityIndicator
                                animating={loadPostVote}
                                size="small" color="#0000ff" />
                            <Button
                                success
                                style={{
                                    alignItems: 'center',
                                    alignSelf: 'center',
                                    paddingStart: 20,
                                    paddingEnd: 20,
                                    marginTop: 10
                                }}
                                disabled={btnVote}
                                onPress={() => this.handleVote()}
                            >
                                <Text style={{ color: '#fff', fontWeight: 'bold' }}>Yakin!</Text>
                            </Button>
                            <Text
                                style={{
                                    alignSelf: 'center',
                                    textAlign: 'center',
                                    margin: 15,
                                    fontSize: 12,
                                    textDecorationLine: 'underline'
                                }}
                                onPress={() => this.handleCancel()}
                            >
                                Saya ingin melihat calon lain
                            </Text>
                        </View>
                    </Modal>
                </View>

                <View>
                    <Modal
                        isVisible={modalError}
                        onBackdropPress={() => this.setState({ modalError: false })}
                        backdropOpacity={0.8}
                        animationIn="zoomInDown"
                        animationOut="zoomOutUp"
                        animationInTiming={2000}
                        animationOutTiming={1000}
                        backdropTransitionInTiming={600}
                        backdropTransitionOutTiming={600}
                    >
                        <View style={{
                            backgroundColor: 'white',
                            borderRadius: 10,
                            padding: 15
                        }}>
                            <Text
                                style={{
                                    alignSelf: 'center',
                                    textAlign: 'center',
                                    margin: 10
                                }}
                            >
                                {messageError}
                            </Text>
                            <Button
                                primary
                                style={{
                                    alignItems: 'center',
                                    alignSelf: 'center',
                                    paddingStart: 20,
                                    paddingEnd: 20,
                                    marginTop: 10
                                }}
                                onPress={() => this.handleSuccessVote()}
                            >
                                <Text style={{ color: '#fff', fontWeight: 'bold' }}>Baiklah!</Text>
                            </Button>
                        </View>
                    </Modal>
                </View>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    centered: {
        alignItems: 'center'
    }
});

export default VoteScreen