import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, StatusBar, Dimensions, Image, Animated, Platform, BackHandler } from 'react-native';
import { Container, Button, Drawer, List, ListItem, Content } from 'native-base';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import Feather from 'react-native-vector-icons/Feather';
import { handleGetItem } from '../../components/storeAsync.storage';

class DashboardScreen extends React.Component {
    constructor(props) {
        super(props);
        this._renderItem = this._renderItem.bind(this);
        this._isMounted = false;
        this.state = {
            //Bila Sudah Login
            //set isLogined true pada asyncstorage
            //Saat sudah terload baru pindah ke Dashboard
            //Data di inputkan ke AsyncStorage
            //Agar aplikasi berjalan smooth
            isOpen: false,
            activeIndex: 0,
            carouselItems: [],
            nama: '',
            nisn: '',
            bgMain: '#0F9797'
        }
    }

    componentDidMount() {
        this._isMounted = true;
        this._isMounted && this.getDataListener();
    }

    async getDataListener(){
        try {
            await handleGetItem('currentCandidate')
                .then(x => {
                    this.setState({
                        carouselItems: x
                    })
                }).catch(err => {
                    console.log(err)
                })

            await handleGetItem('currentUser')
                .then(x => {
                    this.setState({
                        nama: x.payload.nama,
                        nisn: x.payload.nisn
                    })
                }).catch(err => {
                    console.log(err)
                })
        } catch (e) {
            console.log('Dashboard')
            console.log(e)
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
     }

    _renderItem({ item, index }) {
        return (
            <Animated.View>
                <TouchableOpacity
                    style={{
                        backgroundColor: '#fff',
                        borderRadius: 5,
                        height: Dimensions.get('window').height / 1.8,
                        padding: 15,
                        margin: 15,
                        alignItems: 'center',
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 12,
                        },
                        shadowOpacity: 0.58,
                        shadowRadius: 13.00,
                        elevation: 24
                    }}
                    onPress={() => this.props.navigation.navigate('DetailKandidat', { userId: item.id, nama: item.nama, image: item.image, title: item.nama })}
                >
                    <Image
                        source={{ uri: item.image }}
                        style={{ width: 200, height: 200, borderRadius: 200 / 2 }}
                    />
                    <Text style={{ fontSize: 20, margin: 15, fontFamily: 'GoogleSans-Regular', textAlign: 'center' }} >{item.nama}</Text>
                    <View
                        style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: 'flex-end',
                            alignItems: 'center'
                        }}
                    >
                        <Text style={{ marginBottom: 15, fontSize: 13 }}>{item.footer}</Text>
                        <View
                            style={{
                                alignItems: 'center',
                                borderWidth: 1,
                                borderColor: 'black',
                                borderRadius: 3,
                                padding: 5,
                                width: Dimensions.get("screen").width / 2,
                                backgroundColor: '#089895'
                            }}
                        >
                            <Text style={{ color: '#fff', marginLeft: 10, marginRight: 10 }}>Lihat Kampanye</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </Animated.View>
        )
    }

    render() {
        return (
            <Container style={{ backgroundColor: this.state.bgMain }}>
                <StatusBar translucent backgroundColor="transparent" barStyle="light-content" />
                <View
                    style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'space-between',
                    }}
                >
                    <View
                        style={{
                            borderRadius: 10,
                            height: 80,
                            margin: 10,
                            marginTop: getStatusBarHeight(),
                        }}
                    >
                        <Grid>
                            <Col>
                                <Row size={1} style={{ alignItems: 'flex-end' }}>
                                    <Text style={{ fontFamily: 'GoogleSans-Regular', fontSize: 16, color: '#fff' }}>Selamat Datang,</Text>
                                </Row>
                                <Row size={1}>
                                    <Text style={{ fontFamily: 'ProductSans-Bold', fontSize: 24, color: '#bef2eb' }}>{this.state.nama}</Text>
                                </Row>
                            </Col>
                            <Col style={{ alignItems: 'flex-end', justifyContent: 'center' }}>
                                <View>
                                    <TouchableOpacity
                                        style={{
                                            height: 50,
                                            width: 50,
                                            borderRadius: 50 / 2,
                                            margin: 10,
                                            alignContent: 'center',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            backgroundColor: '#e4f6f6'
                                        }}
                                        onPress={() => this.props.navigation.openDrawer()}
                                    >
                                        <Feather name="menu" size={24} color="#1d959e" />
                                    </TouchableOpacity>
                                </View>
                            </Col>
                        </Grid>
                    </View>
                    <Text style={Platform.OS === 'ios' ? styles.titleIos : styles.titleAndroid}>Calon Ketua FOJB</Text>
                    <Carousel
                        ref={(c) => { this._carousel = c }}
                        data={this.state.carouselItems}
                        renderItem={this._renderItem}
                        sliderWidth={Dimensions.get('window').width}
                        itemWidth={300}
                        onSnapToItem={(index) => this.setState({ activeIndex: index })}
                    />

                    <Pagination
                        dotsLength={this.state.carouselItems.length}
                        activeDotIndex={this.state.activeIndex}
                        dotStyle={{
                            width: 10,
                            height: 10,
                            borderRadius: 5,
                            marginHorizontal: 8,
                            backgroundColor: '#fff'
                        }}
                        inactiveDotStyle={{
                            width: 10,
                            height: 10,
                            borderRadius: 5,
                            marginHorizontal: 8,
                            backgroundColor: '#fff'
                        }}
                        inactiveDotOpacity={0.4}
                        inactiveDotScale={0.6}
                    />
                </View>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    centered: {
        alignItems: 'center'
    },
    titleAndroid: {
        fontSize: 24,
        padding: 10,
        fontFamily: 'ProductSans-Bold',
        alignSelf: 'center',
        color: '#fff'
    },
    titleIos: {
        fontSize: 24,
        padding: 10,
        fontFamily: 'ProductSans-Bold',
        alignSelf: 'center',
        color: '#fff'
    }
});

export default DashboardScreen