import axios from 'axios';
import { apiPostVote } from '../helpers/ApiStore'
import { HeaderApp, headerUser } from '../helpers/HeaderApp'
import AsyncStorage from '@react-native-community/async-storage';

export const HttpService = {
    getData,
    postVote
};

//GET DATA
function getData(apiGet) {
    const requestOptions = { timeout: 15000, headers: HeaderApp() };
    return axios.get(apiGet, requestOptions)
        .then(data => {
            return data;
        })
        .catch(error => {
            const errors = 'Tidak Ada Koneksi Internet';
            return Promise.reject(errors);
        });
}

//POST DATA
async function postVote(data) {
    var token = await AsyncStorage.getItem('currentToken')
    if (token !== null) {
        var tokenJadi = JSON.parse(token);
    }

    const requestOptions = { headers: 'token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBOYW1lIjoiRVZvdGluZyIsInVzZXJJZCI6MSwiaWF0IjoxNTk3MzA5NjEyfQ.1HdZUmNnA52ORqRZQlLIO3DNq4bfXaJdJmItFEeqxto' };
    return await axios.post("http://192.168.100.153:8080/api/voteapp/vote", { "idCandidate" : "1" } , { headers: 'token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBOYW1lIjoiRVZvdGluZyIsInVzZXJJZCI6MSwiaWF0IjoxNTk3MzA5NjEyfQ.1HdZUmNnA52ORqRZQlLIO3DNq4bfXaJdJmItFEeqxto' })
        .then(user => {
            return user;
        })
        .catch(error => {
            if (error.response) {
                if (error.response.status == 409) {
                    return Promise.reject(error.response.data)
                }

                if (error.response.status == 403) {
                    const message = 'Tidak Diberi Akses!';
                    return Promise.reject(message)
                }
            }

            return Promise.reject(error)
        });
}