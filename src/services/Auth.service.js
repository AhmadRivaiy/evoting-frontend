import axios from 'axios';
import { apiLogin, apiPostLogin, apiValidedLogin, apiPostVote } from '../helpers/ApiStore'
import { handleError } from '../helpers/HandleError';

export const authService = {
    AuthUser,
    postLogin,
    checkingVoted
};

//POST DATA
function AuthUser(no_tlp) {
    return axios.post(apiLogin, {
        no_tlp: no_tlp
      })
    .then(user => {
        return user.data;
    })
    .catch(handleError)
}

function postLogin(data) {
    return axios.post(apiPostLogin, data)
        .then(user =>{
            return user;
        })
        .catch(error => {
            if (error.response) {
                if(error.response.status == 409){
                    return Promise.reject(error.response.data)
                }
            }

            return Promise.reject(error)
        });
}

function checkingVoted(data) {
    return axios.post(apiValidedLogin, data)
    .then(user => {
        return user.data;
    })
    .catch(error => {
        if ([401, 403].indexOf(error.response.status) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            const error = 'Nomor Telepon Tidak Diperbolehkan Di Perangkat Ini';
            return Promise.reject(error);
        }
    })
}