import AsyncStorage from '@react-native-community/async-storage';

export async function handleGetItem(keyObject) {
    try {
        getItem    = await AsyncStorage.getItem(keyObject);
        nextData   = JSON.parse(getItem);
        return nextData;
    }catch (error) {
        console.log('err store')
        return error;
    }
}