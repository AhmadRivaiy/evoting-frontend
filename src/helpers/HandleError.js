export function handleError(error) {
    if (error.response) {
        if ([401, 403].indexOf(error.response.status) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            const error = 'Nomor Telepon Tidak Terdaftar!';
            return Promise.reject(error);
        }

        if ([422].indexOf(error.response.status) !== -1) {
            const error = 'Nomor Telepon Salah!';
            return Promise.reject(error);
        }

        if ([404].indexOf(error.response.status) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            const error = 'Tidak Ada Koneksi Internet';
            return Promise.reject(error);
        }

        if ([500].indexOf(error.response.status) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            const error = 'Kesalahan Pada Server!';
            return Promise.reject(error);
        }
    }else if(error){
        const error = 'Kesalahan Pada Server!';
        return Promise.reject(error);
    }

    return error;
}