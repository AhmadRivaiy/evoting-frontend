import AsyncStorage from '@react-native-community/async-storage';

export function HeaderApp() {
    // return authorization header with jwt token
    return { 'x-access-token': 'token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBOYW1lIjoiRVZvdGluZyIsImlhdCI6MTU5NzExNjU1N30.iYqdypPh26Sfha489bCj81qAKHmEmqF4LUgYIdwGWBI' };
}

export async function headerUser() {
    // return authorization header with jwt token
    try {
        var token = await AsyncStorage.getItem('currentToken')
        if(token !== null) {
            var tokenJadi = JSON.parse(token);
            console.log(tokenJadi)
            return { 'x-access-token': 'token=' + tokenJadi.tokenIs };
        }
    } catch(e) {
        console.log(e)
    }
}