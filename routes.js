import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';
import drawerContentComponents from './DrawerComponent'

//Pages
import SplashScreen from './src/views/pages/SplashScreen'
import LoginScreen from './src/views/pages/LoginScreen'
import WelcomeScreen from './src/views/pages/WelcomeScreen'
import QuickCount from './src/views/pages/QuickCount'

//Dashboard
import DashboardScreen from './src/views/dashboard/DashboardScreen';

//Screen
import DetailKandidatScreen from './src/views/screens/DetailKandidat';
import VoteScreen from './src/views/pages/VoteScreen';
import { Right } from 'native-base';
import { Dimensions } from 'react-native';

const MainScreenNavigator = createDrawerNavigator({
  Home: { screen : DashboardScreen,
    navigationOptions: {
      headerShown: false
    }
  }
},
{
  contentComponent: drawerContentComponents,
  drawerPosition: 'right',
  drawerWidth: Dimensions.get('screen').width/1.2
});

const VotingApp = createStackNavigator({
    Splash: { screen: SplashScreen,
      navigationOptions: {
        headerShown: false
      }
    },
    Login: { screen : LoginScreen,
      navigationOptions: {
        headerShown: false
      }
    },
    Welcome: { screen : WelcomeScreen,
      navigationOptions: {
        headerShown: false
      }
    },
    Dashboard: { screen : MainScreenNavigator,
      navigationOptions: {
        headerShown: false
      }
    },
    DetailKandidat: { screen : DetailKandidatScreen,
      navigationOptions: {
        headerShown: true,
        title: false,
        headerTransparent: true,
        headerTintColor: 'white'
      }
    },
    Vote: { screen : VoteScreen,
      navigationOptions: {
        headerShown: true,
        title: false,
        headerTransparent: true
      }
    },
    QuickCount: { screen : QuickCount,
      navigationOptions: {
        headerShown: true,
        title: 'Quick Count',
        headerTransparent: true,
        headerTintColor: 'white'
      }
    },
  },
  {
    initialRouteName: 'Splash'
  })
  
  export default createAppContainer(VotingApp);